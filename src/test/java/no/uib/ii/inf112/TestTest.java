package no.uib.ii.inf112;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import java.util.Arrays;

public class TestTest {
	TextAligner aligner = new TextAligner() {

		public String center(String text, int width) {
			int extra = (width - text.length()) / 2;
			return " ".repeat(extra) + text + " ".repeat(extra);
		}

		public String flushRight(String text, int width) {
			return " ".repeat(width - text.length()) + text;
		}

		public String flushLeft(String text, int width) {
			return text + " ".repeat(width - text.length());
		}

		public String justify(String text, int width) {
			if (text.length() > width)
				throw new IllegalArgumentException();

			var words = text.split(" ");
			StringBuilder sb = new StringBuilder();
			int total = 0;
			for (var w : words) {
				total += w.length();
			}
			int i = (width - total) / (words.length - 1);
			for (String word : words) {
				sb.append(word + " ".repeat(i));
			}
			return sb.toString().strip();
		}};
		
	@Test
	void test() {
//		testCenter();
	}

	@Test
	void testCenter() {
		assertEquals("  A  ", aligner.center("A", 5));
		assertEquals("  A  ", aligner.center("A", 6));
		assertEquals(" foo ", aligner.center("foo", 5));
	}

	@Test
	void testRight() {
		assertEquals("    A", aligner.flushRight("A", 5));
		assertEquals(" asdf", aligner.flushRight("asdf", 5));
	}

	@Test
	void testLeft() {
		assertEquals("A    ", aligner.flushLeft("A", 5));
		assertEquals("asdf ", aligner.flushLeft("asdf", 5));
	}

	@Test
	void testJusify() {
		assertEquals("fee   fie   foo", aligner.justify("fee fie foo", 15));
		assertEquals("fee   fie   foo", aligner.justify("fee fie foo", 16));
		assertEquals("fee  asdfasdf  foo", aligner.justify("fee asdfasdf foo", 18));
	}
}
